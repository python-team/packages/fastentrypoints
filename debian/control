Source: fastentrypoints
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: William Grzybowski <william@grzy.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools
Standards-Version: 4.6.1
Homepage: https://github.com/ninjaaron/fast-entry_points
Vcs-Browser: https://salsa.debian.org/python-team/packages/fastentrypoints
Vcs-Git: https://salsa.debian.org/python-team/packages/fastentrypoints.git
Testsuite: autopkgtest-pkg-python

Package: fastep
Architecture: all
Section: utils
Depends: python3-fastentrypoints, ${misc:Depends}, ${python3:Depends}
Description: Adjust Python project to use fastentrypoints
 fastep will make sure fastentrypoints is included in MANIFEST.in and present
 in setup.py.
 .
 fastentrypoints make entry_points specified in setup.py load more quickly.

Package: python3-fastentrypoints
Architecture: all
Depends: python3-setuptools, ${misc:Depends}, ${python3:Depends}
Suggests: fastep
Description: Make entry_points specified in setup.py load more quickly
 Using entry_points in your setup.py makes scripts that start really slowly
 because it imports pkg_resources, which is a horrible thing to do if you
 want your trivial script to execute more or less instantly.
 .
 fastentrypoints aims to fix that bypassing pkg_resources, making scripts
 load a lot faster.
